﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public int maxPlatforms = 20;
    public GameObject platform;
    public float horizontalMin = 7f;
    public float horizontalMax = 12f;
    public float verticalMin = -6f;
    public float verticalMax = 6;

    private Vector2 originPosition;

    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.position;
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Spawn()
    {
        for (int i = 0; i < maxPlatforms; i++)
        {
            Vector2 randomPosition = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax)); //genereate a random location offset a certain ammount from the last one
            Instantiate(platform, randomPosition, Quaternion.identity);
            originPosition = randomPosition;
        }
    }
}
